"irl Required:
call plug#begin()

Plug 'guns/vim-sexp'
Plug 'cstrahan/vim-capnp'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'

Plug 'vim-scripts/oberon.vim'
Plug 'jrozner/vim-antlr'
Plug 'vim-syntastic/syntastic'
let g:syntastic_mode_map = { 'passive_filetypes': ['javascript'] }

Plug 'LnL7/vim-nix'
Plug 'cespare/vim-toml'
Plug 'lepture/vim-jinja'
Plug 'matze/vim-lilypond'
Plug 'igankevich/mesonic'

" (Optional) Multi-entry selection UI.
Plug 'junegunn/fzf'

" Vim Tooling
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'Shougo/vimproc.vim', {'do' : 'make'}
Plug 'Xuyuanp/nerdtree'
Plug 'bling/vim-airline'
Plug 'bronson/vim-trailing-whitespace'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'godlygeek/tabular'
Plug 'ludovicchabant/vim-lawrencium'
Plug 'neomake/neomake'
Plug 'scrooloose/nerdcommenter'
Plug 'tpope/vim-fugitive'
" Plug 'vim-scripts/paredit.vim'
" Other
Plug 'majutsushi/tagbar'
Plug '~/bonjour/vim'
Plug 'dylan-lang/dylan-vim'

let g:deoplete#enable_at_startup = 0

call plug#end()

" Better copy & paste

set pastetoggle=<F2>
set clipboard=unnamed

set mouse=a " on OSX press alt and click

" Do not loose selection when indenting with < or >

vnoremap < <gv
vnoremap > >gv

" folding magic
" =========================
" When opening the file, unfold all. Fold all with zM
" au BufRead * normal zR
" set foldmethod=indent
set foldnestmax=2
nnoremap <space> zA
vnoremap <space> zA

autocmd FileType cs setlocal expandtab shiftwidth=2 tabstop=2
autocmd FileType lua setlocal expandtab shiftwidth=4 tabstop=4
autocmd FileType yaml setlocal expandtab shiftwidth=2 tabstop=2

" Python
autocmd FileType python setlocal expandtab shiftwidth=2 tabstop=2 formatoptions=croqj
autocmd FileType python setlocal softtabstop=2 textwidth=80 comments=:#\:,:#
autocmd FileType scheme setlocal expandtab



" Javacript
autocmd FileType javascript setlocal expandtab shiftwidth=2 tabstop=2

" other

autocmd FileType html setlocal expandtab shiftwidth=2 tabstop=2
autocmd FileType vim setlocal expandtab shiftwidth=2 tabstop=8 softtabstop=2
autocmd FileType xml setlocal expandtab shiftwidth=1 tabstop=1
autocmd FileType docbk setlocal expandtab shiftwidth=1 tabstop=1

syntax on

set nocompatible   " Disable vi-compatibility
set laststatus=2   " Always show the statusline
set encoding=utf-8 " Necessary to show Unicode glyphs


syntax enable

inoremap jj <Esc>
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

" Disable Arrow keys in Insert mode
imap <up> <nop>
imap <down> <nop>
imap <left> <nop>
imap <right> <nop>

" term

"nnoremap <silent> <f6> :REPLSendLine<cr>
"vnoremap <silent> <f6> :REPLSendSelection<cr>

"command! -range=% REPLSendSelection call REPLSend(s:GetVisual())
"command! REPLSendLine call REPLSend([getline('.')])

"function! REPLSend(lines)
  "call jobsend(g:last_term_job_id, add(a:lines, ''))
"endfunction

"function! s:GetVisual()
  "let [lnum1, col1] = getpos("'<")[1:2]
  "let [lnum2, col2] = getpos("'>")[1:2]
  "let lines = getline(lnum1, lnum2)
  "let lines[-1] = lines[-1][:col2 - 2]
  "let lines[0] = lines[0][col1 - 1:]
  "return lines
"endfunction


" airline
let g:airline#extensions#tabline#enabled = 1
set laststatus=2

set hidden

let backupdir="~/.config/nvim/backup"

let g:python3_host_prog='~/.guix-profile/bin/python3'

set autoindent
set backspace=indent,eol,start
set complete-=i
set smarttab
autocmd! BufWritePost * Neomake

let g:deoplete#enable_at_startup=1

autocmd FileType ruby setlocal sw=2 et ts=2
autocmd FileType xml setlocal sw=2 et ts=2
autocmd FileType st setlocal sw=4 et ts=4
autocmd FileType vue setlocal sw=2 et ts=2
autocmd FileType markdown setlocal tw=60 expandtab shiftwidth=4 tabstop=4
autocmd FileType tex setlocal tw=60 et ts=2 sw=2
autocmd FileType pascal setlocal tw=100 et ts=4 sw=4
autocmd FileType c setlocal ts=8 noet sw=8 ts=8
autocmd FileType d setlocal ts=2 sw=2 et
autocmd FileType dylan setlocal ts=2 sw=2 et
autocmd FileType scheme set lispwords+=define-module,define-record-type,syntax-rules,syntax-case,let-values,let*-values

set nobackup

autocmd FileType markdown syn match markdownLeanCode '^<<(.*)$'
autocmd FileType markdown hi def link markdownLeanCode markdownH1

command WC call WC()

set lispwords+=define-class,define-method
